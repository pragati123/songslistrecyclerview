package com.example.poko.recyclerviwdemo;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Poko on 29-06-2016.
 */
public class SelectedSongViewHolder extends RecyclerView.ViewHolder {

    private Songs selectedcurrentSong;

    private TextView selectedsongtitleTextView;
    private TextView selectedsongdescriptionTextView;
    public SelectedSongViewHolder(View itemView) {
        super(itemView);

        selectedsongtitleTextView = (TextView) itemView.findViewById(R.id.selectedsongsong_title);
        selectedsongdescriptionTextView = (TextView) itemView.findViewById(R.id.selectedsongsong_album);

    }

    public void bindSong(Songs song) {
        this.selectedcurrentSong = song;
        selectedsongtitleTextView.setText(song.getTitle());
        selectedsongdescriptionTextView.setText(song.getArtist());

    }
}
