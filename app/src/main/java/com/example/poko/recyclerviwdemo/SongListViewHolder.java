package com.example.poko.recyclerviwdemo;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Poko on 20-06-2016.
 */
public class SongListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private Songs currentSong;
    public holderSelectedSelected holderListner;

    private TextView titleTextView;
    private TextView descriptionTextView;


    public SongListViewHolder(View itemView) {
        super(itemView);

        titleTextView = (TextView) itemView.findViewById(R.id.song_title);
        descriptionTextView = (TextView) itemView.findViewById(R.id.song_album);
        itemView.setOnClickListener(this);

    }

    public void bindSong(Songs song) {
        this.currentSong = song;
        titleTextView.setText(song.getTitle());
        descriptionTextView.setText(song.getArtist());

    }

    @Override
    public void onClick(View v) {


        this.holderListner.songSelectedFromHolder(currentSong);


    }

    public interface holderSelectedSelected {
        public void songSelectedFromHolder(Songs song);

    }


}
