package com.example.poko.recyclerviwdemo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Poko on 29-06-2016.
 */
public class SelectedSongAdapter extends RecyclerView.Adapter<SelectedSongViewHolder> {

    @Override
    public SelectedSongViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.activity_selected_songs_activity, null);
        return new SelectedSongViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SelectedSongViewHolder holder, int position) {
        Songs song = SelectedElementsActivity.recievedSongs.get(position);
        holder.bindSong(song);
    }

    @Override
    public int getItemCount() {
        return SelectedElementsActivity.recievedSongs.size();
    }
}


