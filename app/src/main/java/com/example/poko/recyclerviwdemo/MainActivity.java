package com.example.poko.recyclerviwdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SongRecyclerViewAdapter.adapterSelectedSelected {

    private RecyclerView songsListRecycleView;
    private Button multiselectButton;
    private Button doneButton;
    public  List<Songs> selectedSongs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        selectedSongs = new ArrayList<Songs>();

        multiselectButton = (Button) findViewById(R.id.multiselect_button);
        doneButton = (Button) findViewById(R.id.done_button);


        multiselectButton.setOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View v) {
                                                     if (v.getId() == R.id.multiselect_button) {
                                                         doneButton.setVisibility(View.VISIBLE);
                                                     }
                                                 }
                                             }

        );

        doneButton.setOnClickListener(this);
        songsListRecycleView = (RecyclerView)

                findViewById(R.id.songsList_recycle);

        songsListRecycleView.setLayoutManager(new

                LinearLayoutManager(this)

        );

        SongRecyclerViewAdapter adapter = new SongRecyclerViewAdapter();
        adapter.adapterListner = this;
        songsListRecycleView.setAdapter(adapter);
    }


    @Override
    public void onClick(View v) {
        Intent intent= SelectedElementsActivity.newIntent(this,selectedSongs);
        startActivity(intent);

    }


    @Override
    public void songSelectedFromadapter(Songs song) {

        if (song.getIsSelected()) {
            selectedSongs.remove(song);
            song.setIsselected(false);

        } else {
            selectedSongs.add(song);
            song.setIsselected(true);

        }
        for (int i = 0; i < selectedSongs.size(); i++)
            Log.d("Codekamp", "no of selected songs is " + selectedSongs.get(i));

    }
}
