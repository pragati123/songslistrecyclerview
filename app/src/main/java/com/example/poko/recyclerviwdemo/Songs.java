package com.example.poko.recyclerviwdemo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Poko on 17-06-2016.
 */
public class Songs {
    private String title;
    private String artist;
    private int id;
    public  boolean songExist = false;
    private static int totalSong = 0;

    public Songs(String title, String artist) {
        totalSong++;
        this.id = totalSong;
        this.songExist = false;
        this.title = title;
        this.artist = artist;
    }

    public String getTitle() {

        return title;
    }

    public String getArtist() {
        return artist;
    }

    public int getId() {
        return id;
    }
    public boolean getIsSelected(){
        return songExist;
    }

    public void setIsselected( boolean value){
        this.songExist=value;
    }

    public static List<Songs> allSongs() {
        List<Songs> songs = new ArrayList<>();
        /*songs.add(new Songs("Bol Na", "Arijit Singh"));
        songs.add(new Songs("Na Na", "Akon"));
        songs.add(new Songs("Kal ho na ho", "Sonu"));
        songs.add(new Songs("Kabira", "Arijit Singh"));*/

        for (int i = 0; i < 100; i++) {
            songs.add(new Songs("New songs" + i, "Artist" + i));
        }

        return songs;

    }
}
