package com.example.poko.recyclerviwdemo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Poko on 20-06-2016.
 */
public class SongRecyclerViewAdapter extends RecyclerView.Adapter<SongListViewHolder> implements SongListViewHolder.holderSelectedSelected {


    public adapterSelectedSelected adapterListner;

    @Override
    public SongListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.songs_row, null);
        return new SongListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SongListViewHolder holder, int position) {
        Songs song = Songs.allSongs().get(position);
        holder.holderListner = (this);

        holder.bindSong(song);

    }

    @Override
    public int getItemCount()

    {
        return Songs.allSongs().size();
    }

    @Override
    public void songSelectedFromHolder(Songs song) {
        this.adapterListner.songSelectedFromadapter(song);
    }

    public interface adapterSelectedSelected {
        public void songSelectedFromadapter(Songs song);

    }
}
