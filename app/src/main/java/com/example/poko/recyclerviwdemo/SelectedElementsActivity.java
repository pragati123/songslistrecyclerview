package com.example.poko.recyclerviwdemo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class SelectedElementsActivity extends AppCompatActivity {

    private RecyclerView selectedsongsRecyclerView;
    public static ArrayList<Songs> recievedSongs ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_elements);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
       recievedSongs = (ArrayList<Songs>) args.getSerializable("ARRAYLIST");



        selectedsongsRecyclerView = (RecyclerView) findViewById(R.id.selectedsongsList_recycle);

        selectedsongsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        selectedsongsRecyclerView.setAdapter(new SelectedSongAdapter());
    }

    public static Intent newIntent(Context context, List<Songs> songs) {
        Intent intent = new Intent(context, SelectedElementsActivity.class);

        Bundle args = new Bundle();
        args.putSerializable("ARRAYLIST",(Serializable)songs);
        intent.putExtra("BUNDLE",args);

        return intent;
    }
}
